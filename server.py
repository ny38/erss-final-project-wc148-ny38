import socket
import sys
import psycopg2
from  threading import Thread
from _thread import *
import time
import gl
import world_amazon_pb2
import amazon_ups_pb2
from worldConn import WorldConnector
from upsConn import UPSConnector
from databaseConn import DatabaseConnector
from google.protobuf.internal.decoder import _DecodeVarint32
from google.protobuf.internal.encoder import _EncodeVarint

conn = psycopg2.connect("dbname = 'Amazon' user = 'postgres' password = 'passw0rd'")
WORLD_HOST = gl.worldhost
WORLD_PORT = gl.worldport


def world(conn):
        world = WorldConnector(conn)
        world.connect_world()
        world.sender_receiver_setup()
        time.sleep(5)
        t1 = Thread(target= world.handle, args=())
        t1.start()
        t2 = Thread(target = world.receiver.handle, args = ())
        t2.start()
        t3 = Thread(target = world.sender.handle, args = ())
        t3.start()   
        t1.join()
        t2.join()
        t3.join()


def ups(conn):
        ups = UPSConnector(conn)
        ups.connect()
        ups.accept_ups()
        print("ups connects me")
        t1 = Thread(target= ups.handle, args=())
        t1.start()
        t2 = Thread(target = ups.recv_handler, args=())
        t2.start()
        t1.join()
        t2.join()

def init_local_db():
            conn = psycopg2.connect("dbname = 'Amazon' user = 'postgres' password = 'passw0rd'")
            cur = conn.cursor()
            cur.execute("DROP TABLE IF EXISTS orders")
            cur.execute("DROP TABLE IF EXISTS warehouse")
            cur.execute("DROP TABLE IF EXISTS requests")
            cur.execute("DROP TABLE IF EXISTS responses")
            conn.commit()
            cur.execute("CREATE TABLE warehouse("
                        "itemid bigint PRIMARY KEY,"
                        "quantity integer,"
                        "location integer[2],"
                        "description text);")
            cur.execute("CREATE TABLE orders("
                        "packageid serial PRIMARY KEY,"
                        "itemid bigint,"
                        "quantity integer,"
                        "description text,"
                        "upsid text,"
                        "des integer[2] NOT NULL,"
                        "pack text DEFAULT 'unpacked',"
                        "truckid integer DEFAULT -1,"
                        "deliver text DEFAULT 'unloaded',"
                        "psw text DEFAULT '12345678');")
            cur.execute("CREATE TABLE requests("
                         "seq_num bigint PRIMARY KEY);")
            cur.execute("CREATE TABLE responses("
                        "w_seq_num bigint PRIMARY KEY,"
                        "type text);")
            cur.execute("INSERT INTO orders VALUES(default, 1, 20,'Cpp',1000,array[1,2], 'unpacked', -1, 'unloaded')")
            #cur.execute("INSERT INTO orders VALUES(default, 2, 30,'JAVA',666,array[2,3], 'unpacked', -1, 'unloaded')")
            cur.execute("INSERT INTO warehouse VALUES(1,10000,array[0,0],'Cpp')")
            cur.execute("INSERT INTO warehouse VALUES(2,10000,array[0,0],'JAVA')")
            cur.execute("INSERT INTO warehouse VALUES(3,10000,array[0,0],'Go')")
            cur.execute("INSERT INTO warehouse VALUES(4,10000,array[0,0],'Python')")

            conn.commit()
            #sql = ("""INSERT INTO orders(packageid, description, des, upsid) VALUES(%s, %s, %s, %s);"""%(2,'C++',[1,1],666))
            #cur.execute(sql)
            #conn.commit()
            cur.close()


if __name__ == "__main__":
    try:
        init_local_db()
        t1 = Thread(target= world, args=(conn,))
        t1.start()
        t2 = Thread(target = ups, args= (conn,))
        t2.start()
        print("running")
        t1.join()
        t2.join()
    except Exception as e:
        print("Error",e)

