import socket
import string
import world_amazon_pb2
import psycopg2
import sys
import time
import gl
from sender import sender
from receiver import receiver

from google.protobuf.internal.decoder import _DecodeVarint32
from google.protobuf.internal.encoder import _EncodeVarint

WORLD_HOST = gl.worldhost
WORLD_PORT = gl.worldport

# ===========TODO==========
# ==========build the pack new order request string =======


def conn1():
    command = world_amazon_pb2.AConnect()
    command.worldid=1
    init = command.initwh.add()
    init.id = 1
    init.x = 0
    init.y = 0
    command.isAmazon = True
    return command

class WorldConnector:
    def __init__(self, conn):
        self.conn = conn
        self.sock = None
        self.sender = sender()
        self.receiver = receiver()

    def sender_receiver_setup(self):
        self.sender.setup(self.conn, self.sock)
        self.receiver.setup(self.conn, self.sock)

    def init_wh(self):
        id = 1
        while(id<5):
            cmd = world_amazon_pb2.ACommands()
            pur = cmd.buy.add()
            pur.whnum = 1
            with gl.lock:
                pur.seqnum = gl.seq
                gl.seq += 1
            things = pur.things.add()
            things.id = id
            if(id==1):
                things.description = "Cpp"
            elif(id==2):
                things.description = "JAVA"
            elif(id==3):
                things.description = "Go"
            else:
                things.description = "Python"
            things.count = 10000
            a = pur.seqnum
            print("init wh:",cmd)
            gl.q.put((a,cmd))
            id+=1
        print("Init world warehouse done")


    def handle(self):
        self.init_wh()
        time.sleep(6)
        while (1):
            self.pack_new_order()
            self.load_unloaded()
            self.check_reconnect()
            time.sleep(3)

    # TABLE orders : packageid  itemid  quantity description  upsid  des  pack  truckid   deliver 
    # ============put on truck==========
    def load_unloaded(self):
            # ==== select unloaded order ====
            sql = """SELECT * FROM orders WHERE pack='packed' AND truckid>0 AND deliver='unloaded';"""
            cur = self.conn.cursor()
            cur.execute(sql)
            results = cur.fetchall()
            if results:
                # put a request in the sender queue
                for result in results:
                    seq,load = self.make_load_request(result[7],result[0])
                    gl.q.put((seq,load))
                    self.update_order(result[0],"loading")
            cur.close()


    def make_load_request(self, truckid,shipid):
        cmd = world_amazon_pb2.ACommands()
        load = cmd.load.add()
        load.whnum = 1
        load.truckid = truckid
        load.shipid = shipid
        with gl.lock:
            load.seqnum = gl.seq
            gl.seq+=1
        return load.seqnum, cmd
    
    def pack_new_order(self):
            sql = """SELECT * FROM orders WHERE pack= 'unpacked';"""
            cur = self.conn.cursor()
            cur.execute(sql)
            results = cur.fetchall()
            if results:
                for result in results:
                    print("FOUND NEW ORDER")
                    cmds = self.make_pack_request(result[0],result[1],result[2])
                    for cmd in cmds:
                        if(cmd!=None):
                            gl.q.put((cmd[0],cmd[1])) #seqnum  request
                    self.update_order(result[0],"packing")
            cur.close()


    def update_order(self,packageid,type):
        if(type=="packing"):
            sql = ("""UPDATE orders SET pack = 'packing' WHERE packageid= %s;"""%(packageid,))
        else:
            sql = ("""UPDATE orders SET deliver = 'loading' WHERE packageid= %s;"""%(packageid,))
        cur = self.conn.cursor()
        cur.execute(sql)
        self.conn.commit()
        cur.close()

    # ============pack new order==========
    def make_pack_request(self,shipid, itemid, count):
            cmds = []
            cmd = world_amazon_pb2.ACommands()
            pack = cmd.topack.add()
            product = pack.things.add()
            #############
            product.id = itemid
            product.count = count
            product.description = self.get_description(itemid)
            ################
            pack.whnum = 1
            pack.shipid = shipid
            with gl.lock:
                pack.seqnum = gl.seq
                gl.seq+=1
            cmds.append((pack.seqnum,cmd))
            cmds.append(self.check_local_inventory(itemid, count))
            return cmds


    def check_local_inventory(self, id, count):
            sql = ("""SELECT quantity FROM warehouse WHERE itemid = %s;""" % (id,))
            cur = self.conn.cursor()
            cur.execute(sql)
            quantity = cur.fetchone()[0]
            if ((quantity - count) < 2000):
                cmd = world_amazon_pb2.ACommands()
                buy = cmd.buy.add()
                things = buy.things.add()
                things.id = id
                things.count = 5000 - (quantity - count)
                things.description = self.get_description(id)
                buy.whnum = 1
                with gl.lock:
                    buy.seqnum = gl.seq
                    gl.seq+=1
                return buy.seqnum, cmd

    def get_description(self, id):
            sql = ("""SELECT description FROM warehouse WHERE itemid = %s;""" % (id,))
            cur = self.conn.cursor()    
            cur.execute(sql)
            return cur.fetchone()[0]


    def recv_data(self, mysock):
        var_int_buff = []
        while True:
            buf = mysock.recv(1)
            var_int_buff += buf
            msg_len, new_pos = _DecodeVarint32(var_int_buff, 0)
            if new_pos != 0:
                break
        whole_message = mysock.recv(msg_len)
        return whole_message

    def connect_world(self):
            world_sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
            world_sock.connect((WORLD_HOST, WORLD_PORT))
            print(WORLD_HOST, WORLD_PORT)
            print("world sock connected")
            # send AConnect
            msg = conn1().SerializeToString()
            hdr = []
            _EncodeVarint(hdr.append, len(msg))
            world_sock.sendall(hdr[0])
            world_sock.sendall(msg)
            print("world connect msg sent")
            # recv response
            whole_message = self.recv_data(world_sock)
            recv = world_amazon_pb2.AConnected()
            recv.ParseFromString(whole_message)
            print(recv)
            self.sock = world_sock
            #print("=======world connected=========")
            print("world sock is:",self.sock)
    
    def check_reconnect(self):
        if (self.sender.reconnect == True):
            self.sock = self.sender.sock
            self.receiver.sock = self.sock
        if(self.receiver.reconnect == True):
            self.sock = self.receiver.sock
            self.sender.sock = self.sock
