import psycopg2
import world_amazon_pb2
import time
import socket
#import server
import gl 
#from worldConn import WorldConnector
from google.protobuf.internal.decoder import _DecodeVarint32
from google.protobuf.internal.encoder import _EncodeVarint

#======== recv from the world and update orders database===========

WORLD_HOST = gl.worldhost
WORLD_PORT = gl.worldport

def conn1():
    command = world_amazon_pb2.AConnect()
    command.worldid=1
    init = command.initwh.add()
    init.id = 1
    init.x = 0
    init.y = 0
    command.isAmazon = True
    return command

class receiver:
    def __init__(self):
        self.conn = None
        self.sock = None
        self.reconnect = False
    
    def setup(self,conn,sock):
        self.conn = conn
        self.sock = sock

    def none_select_exec(self,sql):
        cur = self.conn.cursor()
        cur.execute(sql)
        self.conn.commit()
        cur.close()

    def recv_data(self):
        try:
            var_int_buff = []
            while True:
                buf = self.sock.recv(1)
                var_int_buff += buf
                msg_len, new_pos = _DecodeVarint32(var_int_buff, 0)
                if new_pos != 0:
                    break
            whole_message = self.sock.recv(msg_len)
            return whole_message
        except Exception as e:
            print("error",e)
            


    def handle(self):
        print("receiver running")
        while(1):
                msg = self.recv_data()
                response = world_amazon_pb2.AResponses()
                response.ParseFromString(msg)
                print("RECEIVER :",response)
                for ack in response.acks:
                    self.remove_from_queue(ack)

                for arr in response.arrived: #APurchaseMore
                    self.handle_arrived(arr)

                for packed in response.ready: #APacked
                    self.handle_ready_loaded(packed,'ready')

                for loaded in response.loaded: #ALoaded
                    self.handle_ready_loaded(loaded,'loaded')

                for error in response.error:
                    print("======================")
                    print("ERROR MESSAGE:",error.err)
                    print("origin seq:",error.originseqnum)
                    print("======================")


    def handle_arrived(self,arr):
            seq = arr.seqnum
            exists = self.check_response(seq)
            if(exists==False):
                # execute command
                for thing in arr.things:
                    itemid = thing.id
                    count = thing.count 
                    #update warehouse
                    sql = ("""UPDATE warehouse SET quantity = quantity+ %s WHERE itemid = %s;"""%(itemid,count))
                    self.none_select_exec(sql)
                    # record response &  make ACK
                self.record_response_and_make_ACK(seq)

    
    def handle_ready_loaded(self,res,type):
            id = res.shipid
            seq = res.seqnum
            exists = self.check_response(seq)
            if(exists==False):
                if(type == 'ready'):
                    sql = ("""UPDATE orders SET pack = 'packed' WHERE packageid = %s;"""%(id,))
                else:
                    sql = ("""UPDATE orders SET deliver = 'loaded' WHERE packageid = %s;"""%(id,))
                self.none_select_exec(sql)
                self.record_response_and_make_ACK(seq)


    def check_response(self,w_seq):
        sql = ("""SELECT * FROM responses WHERE w_seq_num = %s;"""%(w_seq,))
        cur = self.conn.cursor()
        cur.execute(sql)
        results = cur.fetchall()
        if results : 
            return True
        else:
            return False

    def record_response_and_make_ACK(self,w_seq):
        sql = ("""INSERT INTO responses (w_seq_num) VALUES(%s);"""%(w_seq,))
        self.none_select_exec(sql)
        cmd = world_amazon_pb2.ACommands()
        cmd.acks.append(w_seq)
        gl.q.put((0,cmd))
            

    # acked request can be removed
    def remove_from_queue(self,ack):
        with gl.lock:
            size = gl.q.qsize()
            for i in range(size):
                notacked = gl.q.get()
                if(ack != notacked[0]):
                    gl.q.put(notacked)
    

        
                    

