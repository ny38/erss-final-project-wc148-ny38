<html lang="en">
<head>
  <title>MY AMAZON</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css">
  <link href='http://fonts.googleapis.com/css?family=Barlow:400,300,700' rel='stylesheet' type='text/css'>
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.0/umd/popper.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.0/js/bootstrap.min.js"></script>
  <style type="text/css">
      body{
        background-color: #fff;
        font-family: 'Barlow', sans-serif !important;
      }
      
      .form{
        margin: 50px auto;
        padding: 25px 20px;
        background: #FF8000;
        box-shadow: 0px 6px 20px #a5a5a5;
        border-radius: 5px;
        color: #fff;
      }
      .form h2{
        margin-top: 0px;
        margin-bottom: 15px;
        padding-bottom: 5px;
        border-radius: 10px;
      }
      .footer{
        padding: 10px;
      }

}
    </style>
</head>
<body>

<nav class="navbar navbar-expand-md navbar-light" style="background: #000;">
  <a class="navbar-brand" href="https://www.amazon.com/">
  	<img src="<c:url value="/images/amazon.png" />" style="width:120px;">
  <span class="navbar-text mr-auto" style = "color: #fff">
    My Amazon (Nibo & Wangke)
  </span>
  </a>
  
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#collapsibleNavbar">
    <span class="navbar-toggler-icon"></span>
  </button>
  <div class="collapse navbar-collapse " id="collapsibleNavbar">
    <ul class="navbar-nav ml-auto">
      <li class="nav-item active">
        <a class="nav-link" href="/login" style = "color: #fff">Home</a>
      </li> 
      <li class="nav-item active">
        <a class="nav-link" href="/inputtrack" style = "color: #fff">Check My Package</a>
      </li>   
    </ul>
    
  </div>  
</nav>
<br>

<div class="container">
      <div class="row">
        <div class="col-md-4 col-sm-12 col-xs-12 offset-md-4">
          <div style="background: #000;" class="bslf form" >
            <form action="/login" method="post" >
              <h2 class="text-center">Please login</h2>       
              <div class="form-group">
                 <input type="text" class="form-control" name="name" placeholder="Username" required="required">
              </div>
              <div class="form-group">
                <input type="password" class="form-control" name="password" placeholder="Password" required="required">
              </div>
              <div class="form-group clearfix">
                <button type="submit" class="btn btn-light float-right">Log in</button>
              </div>
              <div><font color="#fff">${errorMessage}</font></div>
            </form>
            <button  onclick= "location.href='/signup'" type="submit" class="btn btn-light float-right">Sign up</button>
            <br>
            <br>
            <!-- <div  class="form-group clearfix">
            <button  onclick= "location.href='/list-products'" type="submit" class="btn btn-light float-right">Products</button>
            </div> -->
          </div>
        </div>
      </div>
    </div>
<div class="navbar-default fixed-bottom">
      <div class="text-center footer">Copyright � 2019 ERSS Final Project by Nibo Ying and Wangke Chen @ Duke University</div>
</div>
</body>
</html>