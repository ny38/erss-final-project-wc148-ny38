<html lang="en">
<head>
  <title>MY AMAZON</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css">
  <link href='http://fonts.googleapis.com/css?family=Barlow:400,300,700' rel='stylesheet' type='text/css'>
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.0/umd/popper.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.0/js/bootstrap.min.js"></script>
  <style type="text/css">
      body{
        background-color: #fff;
        font-family: 'Barlow', sans-serif !important;
      }
      
      .form{
        margin: 50px auto;
        padding: 25px 20px;
        background: #FF8000;
        box-shadow: 0px 6px 20px #a5a5a5;
        border-radius: 5px;
        color: #fff;
      }
      .form h2{
        margin-top: 0px;
        margin-bottom: 15px;
        padding-bottom: 5px;
        border-radius: 10px;
      }
      .footer{
        padding: 10px;
      }

}
    </style>
</head>
<body>

<nav class="navbar navbar-expand-md navbar-light" style="background: #000;">
  <a class="navbar-brand" href="https://www.amazon.com/">
  	<img src="<c:url value="/images/amazon.png" />" style="width:120px;">
  <span class="navbar-text mr-auto" style = "color: #fff">
    My Amazon (Nibo & Wangke)
  </span>
  </a>
  
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#collapsibleNavbar">
    <span class="navbar-toggler-icon"></span>
  </button>
  <div class="collapse navbar-collapse " id="collapsibleNavbar">
    <ul class="navbar-nav ml-auto">
      <li class="nav-item active">
        <a class="nav-link" href="/list-products" style = "color: #fff">Home</a>
      </li>   
    </ul>
    
  </div>  
</nav>
<br><br>

<div class="container">
  <h2>Order Submitted!</h2>
  <br>
  <form action = "/" method = "GET">
  <table class="table table-bordered table-striped">
			<tbody>
				<tr>
          			<th>Tracking Number (Please write it down before you close the tab)</th>
            		<td><select class="form-control" id="sel1" name="des">
            			<option  name="des" value="${des}" type="text">${track}</option>
            			</select>
            		</td>
            
        		</tr>
        		<tr>
		          	<th>Product Name</th>
		            <td><select class="form-control" id="sel1" name="itemid">
            			<option  name="itemid" value ="${itemid}" type="text">${des}</option>
            			</select>
            		</td>
            		
		        </tr>
		        
		        <tr>
		          	<th>Quantity</th>
		            <td><select class="form-control" id="sel1" name="itemid">
            			<option  name="itemid" value ="${itemid}" type="text">${quantity}</option>
            			</select>
            		</td>
            		
		        </tr>
		        
		        <tr>
		          	<th>UPS ID(Please log into UPS with this ID)</th>
		            <td><select class="form-control" id="sel1" name="itemid">
            			<option  name="itemid" value ="${itemid}" type="text">${upsid}</option>
            			</select>
            		</td>
            		
		        </tr>
		        
		        <tr>
		          	<th>Destination</th>
		            <td><select class="form-control" id="sel1" name="itemid">
            			<option  name="itemid" value ="${itemid}" type="text">[ ${desx},${desy} ] </option>
            			</select>
            		</td>
            		
		        </tr>
		        
			</tbody>
  </table>
 	<!-- <button type="submit" class="btn btn-primary float-right">Download Order</button> -->
  <br>
  </form>
  <a type="button" class="btn btn-light" 
								href="/order-pdf?des=${des}
								&quantity=${quantity}&upsid=${upsid}&desx=${desx}&desy=${desy}">Download PDF</a>
  
  
</div>

</body>
</html>