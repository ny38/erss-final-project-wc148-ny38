<html lang="en">
<head>
  <title>MY AMAZON</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css">
  <link href='http://fonts.googleapis.com/css?family=Barlow:400,300,700' rel='stylesheet' type='text/css'>
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.0/umd/popper.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.0/js/bootstrap.min.js"></script>
  <style type="text/css">
      body{
        background-color: #fff;
        font-family: 'Barlow', sans-serif !important;
      }
      
      .form{
        margin: 50px auto;
        padding: 25px 20px;
        background: #FF8000;
        box-shadow: 0px 6px 20px #a5a5a5;
        border-radius: 5px;
        color: #fff;
      }
      .form h2{
        margin-top: 0px;
        margin-bottom: 15px;
        padding-bottom: 5px;
        border-radius: 10px;
      }
      .footer{
        padding: 10px;
      }

}
    </style>
</head>
<body>

<nav class="navbar navbar-expand-md navbar-light" style="background: #000;">
  <a class="navbar-brand" href="https://www.amazon.com/">
  	<img src="<c:url value="/images/amazon.png" />" style="width:120px;">
  <span class="navbar-text mr-auto" style = "color: #fff">
    My Amazon (Nibo & Wangke)
  </span>
  </a>
  
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#collapsibleNavbar">
    <span class="navbar-toggler-icon"></span>
  </button>
  <div class="collapse navbar-collapse " id="collapsibleNavbar">
    <ul class="navbar-nav ml-auto">
      <li class="nav-item active">
        <a class="nav-link" href="/list-products" style = "color: #fff">Home</a>
      </li>   
      <li class="nav-item active">
        <a class="nav-link" href="/inputtrack" style = "color: #fff">Check My Package</a>
      </li>   
    </ul>
    
  </div>  
</nav>
<br><br>


<div class="container">
  <h2>Product catalog</h2>
  <p>Type something in the input field to search the table for any entry in the table:</p>  
  <input class="form-control" id="myInput" type="text" placeholder="Search..">
  <p>${editmessage.success}</p>
  <p>${editmessage.error }</p>
  <table class="table table-bordered table-striped">
			<thead>
				<tr class="text-white" style="background: #000;">
					<th>Description</th>
					<th>ItemID</th>
					<th></th>
				</tr>
			</thead>
			<tbody id="myTable">
				<c:forEach items="${products}" var="p">
					<tr>
						<td>${p.description}</td>
						<td>${p.itemid}</td>
						<td>
							<a type="button" class="btn btn-light" 
								href="/place-order?des=${p.description}&itemid=${p.itemid}" >Order</a>
						</td>
						
					</tr>
				</c:forEach>
			</tbody>
  </table>
  
</div>

<script>
$(document).ready(function(){
  $("#myInput").on("keyup", function() {
    var value = $(this).val().toLowerCase();
    $("#myTable tr").filter(function() {
      $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
    });
  });
});
</script>





</body>
</html>