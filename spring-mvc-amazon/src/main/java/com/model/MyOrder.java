package com.model;

public class MyOrder {
	private String id;
	private String des;
	private Integer quantity;
	private String upsid;
	private int desx;
	private int desy;
	
	public MyOrder() {}
	
	public String getDes() {
		return des;
	}
	public void setDes(String des) {
		this.des = des;
	}
	public Integer getQuantity() {
		return quantity;
	}
	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}
	public String getUpsid() {
		return upsid;
	}
	public void setUpsid(String upsid) {
		this.upsid = upsid;
	}
	public int getDesx() {
		return desx;
	}
	public void setDesx(int desx) {
		this.desx = desx;
	}
	public int getDesy() {
		return desy;
	}
	public void setDesy(int desy) {
		this.desy = desy;
	}
	
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

}
