package com.model;

public class Organization {
	private String org_name;
	private String phone;
	private String username;
	private String federal_tax_number;
	private boolean verified;
	
	public String getOrg_name() {
		return org_name;
	}
	public void setOrg_name(String org_name) {
		this.org_name = org_name;
	}
	public String getFederal_tax_number() {
		return federal_tax_number;
	}
	public void setFederal_tax_number(String federal_tax_number) {
		this.federal_tax_number = federal_tax_number;
	}
	public boolean isVerified() {
		return verified;
	}
	public void setVerified(boolean verified) {
		this.verified = verified;
	}
	
	public String getPhone() {
		return phone;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	
	
}
