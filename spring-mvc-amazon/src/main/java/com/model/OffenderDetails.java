package com.model;

public class OffenderDetails {
	private String username;
	private String password;
	private String legal_full_name;
	private String age;
	private String type_of_crime;
	private int hours_to_serve;
	private String service_deadline;
	private String skills;
	private String phone;
	
	public OffenderDetails() {
		
	}
	
	public OffenderDetails(OffenderDetails copy) {
		this.setUsername(copy.getUsername());
		this.setPassword(copy.getPassword());
		this.setLegal_full_name(copy.getLegal_full_name());
		this.setAge(copy.getAge());
		this.setType_of_crime(copy.getType_of_crime());
		this.setHours_to_serve(copy.getHours_to_serve());
		this.setService_deadline(copy.getService_deadline());
		this.setSkills(copy.getSkills());
		this.setPhone(copy.getPhone());
	}
	
	public String getPhone() {
		return phone;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getLegal_full_name() {
		return legal_full_name;
	}
	public void setLegal_full_name(String legal_full_name) {
		this.legal_full_name = legal_full_name;
	}
	public String getType_of_crime() {
		return type_of_crime;
	}
	public void setType_of_crime(String type_of_crime) {
		this.type_of_crime = type_of_crime;
	}
	public int getHours_to_serve() {
		return hours_to_serve;
	}
	public void setHours_to_serve(int hours_to_serve) {
		this.hours_to_serve = hours_to_serve;
	}
	public String getService_deadline() {
		return service_deadline;
	}
	public void setService_deadline(String service_deadline) {
		this.service_deadline = service_deadline;
	}
	public String getSkills() {
		return skills;
	}
	public void setSkills(String skills) {
		this.skills = skills;
	}
	public String getAge() {
		return age;
	}
	public void setAge(String age) {
		this.age = age;
	}
	
	
}
