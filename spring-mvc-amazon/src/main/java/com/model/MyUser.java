package com.model;

public class MyUser {
	private String userid;
	private String upsid;
	private String upspassword;
	private int desx;
	private int desy;
	
	public MyUser() {}
	
	
	public String getUserid() {
		return userid;
	}

	public void setUserid(String userid) {
		this.userid = userid;
	}

	public String getUpsid() {
		return upsid;
	}

	public void setUpsid(String upsid) {
		this.upsid = upsid;
	}

	public String getUpspassword() {
		return upspassword;
	}

	public void setUpspassword(String upspassword) {
		this.upspassword = upspassword;
	}

	public int getDesx() {
		return desx;
	}

	public void setDesx(int desx) {
		this.desx = desx;
	}

	public int getDesy() {
		return desy;
	}

	public void setDesy(int desy) {
		this.desy = desy;
	}
	
}
