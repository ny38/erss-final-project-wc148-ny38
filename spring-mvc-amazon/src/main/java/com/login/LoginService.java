package com.login;


import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Service;
import org.springframework.ui.Model;
import org.springframework.web.servlet.ModelAndView;

import com.model.MyOrder;
import com.model.MyUser;
import com.model.OffenderDetails;
import com.model.PdfReportView;
import com.model.Products;


@Service
public class LoginService {
	
	
	public void getmyuser(MyUser myuser, String user) {
		Statement stmt;
		try {
			Class.forName("org.postgresql.Driver");
			Connection c = DriverManager.getConnection(
    		//  change des
            "jdbc:postgresql://vcm-8209.vm.duke.edu:5432/Amazon",
            "postgres", "passw0rd");
			stmt = c.createStatement();
			c.setAutoCommit(false);
			System.out.println("about to exec:");
            String sql = ("SELECT userid,upsid,upspassword,desx,desy FROM users WHERE username ="+"'" +user+"';");
            System.out.println(sql);
            ResultSet rs =  stmt.executeQuery(sql);
            rs.next();
            myuser.setUserid(rs.getString("userid"));
            myuser.setUpsid(rs.getString("upsid"));
            myuser.setUpspassword(rs.getString("upspassword"));
            myuser.setDesx(rs.getInt("desx"));
            myuser.setDesy(rs.getInt("desy"));
            c.commit();
            stmt.close();
            c.close();
		}
		catch ( Exception e ) {
            System.err.println( e.getClass().getName() + ": " + e.getMessage() );
        }
	}
	
	@SuppressWarnings("null")
	public void getStatus(ArrayList<String> ans, int track) {
		Statement stmt;
		try {
			Class.forName("org.postgresql.Driver");
			Connection c = DriverManager.getConnection(
    		//  change des
            "jdbc:postgresql://vcm-8209.vm.duke.edu:5432/Amazon",
            "postgres", "passw0rd");
			stmt = c.createStatement();
			c.setAutoCommit(false);
			System.out.println("about to exec:");
            String sql = ("SELECT pack,truckid,deliver,des FROM orders WHERE packageid = " +track+";");
            System.out.println(sql);
            ResultSet rs =  stmt.executeQuery(sql);
            rs.next();
            ans.add( rs.getString("pack"));
            ans.add( Integer.toString(rs.getInt("truckid")));
            ans.add( rs.getString("deliver"));
            ans.add(rs.getString("des"));
            c.commit();
            stmt.close();
            c.close();
		}
		catch ( Exception e ) {
            System.err.println( e.getClass().getName() + ": " + e.getMessage() );
        }
	}
	
	public boolean validateUser(String user, String password) {
		Statement stmt;
		try {
			Class.forName("org.postgresql.Driver");
			Connection c = DriverManager.getConnection(
    		//  change des
            "jdbc:postgresql://vcm-8209.vm.duke.edu:5432/Amazon",
            "postgres", "passw0rd");
			stmt = c.createStatement();
			c.setAutoCommit(false);
			System.out.println("about to exec:");
            String sql = ("SELECT password FROM users WHERE username ="+"'" +user+"';");
            System.out.println(sql);
            ResultSet rs =  stmt.executeQuery(sql);
            rs.next();
            String p = rs.getString("password");
            System.out.println(p);
            System.out.println(password);
            c.commit();
            stmt.close();
            c.close();
            return p.equals(password);
		}
		catch ( Exception e ) {
            System.err.println( e.getClass().getName() + ": " + e.getMessage() );
            return false;
        }
	}
	
	@SuppressWarnings("null")
	public ModelAndView generatePdf(MyOrder order,HttpServletRequest request, HttpServletResponse response) {
		return (new ModelAndView(new PdfReportView(),"order",order));
	}
	
	public int submit(String des,String itemid, Integer quantity, String upsid,String pw, Integer desx, Integer desy) {
			try {
				Class.forName("org.postgresql.Driver");
				Connection c = DriverManager.getConnection(
	    		//  change des
	            "jdbc:postgresql://vcm-8209.vm.duke.edu:5432/Amazon",
	            "postgres", "passw0rd");
				Statement stmt;
				c.setAutoCommit(false);
				stmt = c.createStatement();
	            String sql = "INSERT INTO orders (packageid,itemid,quantity,description,upsid,des,psw) VALUES(default,"+itemid+","+quantity+","+"'"+des+"'"+",'"+upsid+"',"+"array["+desx+","+desy+"]"+","+pw+");";
	            System.out.println(sql);
	            stmt.executeUpdate(sql);
	            c.commit();
	            stmt.close();
	            c.close();
				return 1;
			}catch ( Exception e ) {
				System.out.println("submit error");
	            System.err.println( e.getClass().getName() + ": " + e.getMessage() );
	            return -1;
	        }
	}
	
	public ArrayList<Products> listproducts() {
		try {
			Class.forName("org.postgresql.Driver");
			Connection c = DriverManager.getConnection(
        		//  change des
                "jdbc:postgresql://vcm-8209.vm.duke.edu:5432/Amazon",
                "postgres", "passw0rd");
			c.setAutoCommit(false);
			return getProfileHelper(c);	
		}catch ( Exception e ) {
            System.err.println( e.getClass().getName() + ": " + e.getMessage() );
            return null;
        }	
	}
	
	public Boolean register(String username, String password,Integer upsid, String upspassword, int desx, int desy) {
		try {
			Class.forName("org.postgresql.Driver");
			Connection c = DriverManager.getConnection(
    		//  change des
            "jdbc:postgresql://vcm-8209.vm.duke.edu:5432/Amazon",
            "postgres", "passw0rd");
			Statement stmt;
			c.setAutoCommit(false);
			stmt = c.createStatement();
            String sql = "INSERT INTO users VALUES(default,"+"'"+username+"'"+","+"'"+password+"'"+","+upsid+","+"'"+upspassword+"'"+","+desx+","+desy+");";
            System.out.println(sql);
            stmt.executeUpdate(sql);
            c.commit();
            stmt.close();
            c.close();
            return true;
		}catch ( Exception e ) {
			System.out.println("submit error");
            System.err.println( e.getClass().getName() + ": " + e.getMessage() );
            return false;
        }
	}
	
	public String track(String itemid, int quantity, String upsid, int desx, int desy) {
		try {
			Class.forName("org.postgresql.Driver");
			Connection c = DriverManager.getConnection(
        		//  change des
                "jdbc:postgresql://vcm-8209.vm.duke.edu:5432/Amazon",
                "postgres", "passw0rd");
			c.setAutoCommit(false);
			return getTracking(c,itemid,quantity,upsid,desx,desy);	
		}catch ( Exception e ) {
            System.err.println( e.getClass().getName() + ": " + e.getMessage() );
            return null;
        }	
	}
	
	private String getTracking(Connection c,String itemid, int quantity, String upsid, int desx, int desy) {
		Statement stmt;
		try {
			stmt = c.createStatement();
			System.out.println("about to exec:");
			//int uid = Integer.valueOf(upsid);
            String sql = ("SELECT packageid FROM orders WHERE itemid =" +itemid+" AND"+" quantity="+quantity+ " AND "+"upsid="+ "'"+upsid+"'"+" AND "+"des="+"array["+desx+","+desy+"];");
            System.out.println(sql);
            ResultSet rs =  stmt.executeQuery(sql);
            rs.next();
            String p = rs.getString("packageid");
            c.commit();
            stmt.close();
            c.close();
            return p;
		}
		catch ( Exception e ) {
            System.err.println( e.getClass().getName() + ": " + e.getMessage() );
        }
		return null;
	}
	
	private ArrayList<Products> getProfileHelper(Connection c) {
		ArrayList<Products> products = new ArrayList<Products>();
		Statement stmt;
		try {
			stmt = c.createStatement();
			System.out.println("about to exec:");
            String sql = "SELECT itemid, description FROM warehouse ORDER BY itemid;";
            ResultSet rs =  stmt.executeQuery(sql);
            while(rs.next()) {
            	Products a = new Products();
            	a.setDescription(rs.getString("description"));
            	a.setItemid(rs.getString("itemid"));
            	System.out.println("getting data:");
            	System.out.println(a.getDescription());
            	System.out.println(a.getItemid());
            	products.add(a);
            }
            c.commit();
            stmt.close();
            c.close();
		}
		catch ( Exception e ) {
            System.err.println( e.getClass().getName() + ": " + e.getMessage() );
        }
		return products;
	}
}

