package com.login;

import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.servlet.ModelAndView;

import com.model.MyOrder;
import com.model.MyUser;


@Controller
@SessionAttributes("name")
public class LoginController {
	
	private MyUser myuser = new MyUser();
	
	private MyOrder order= new MyOrder();
	
	@Autowired
	private LoginService loginService;

	@RequestMapping(value = "/login", method = RequestMethod.GET)
	public String showLoginPage() {
		return "login";
	}
	
	@RequestMapping(value = "/inputtrack", method = RequestMethod.GET)
	public String Page() {
		return "nologinstatus";
	}
	
	@SuppressWarnings("null")
	@RequestMapping(value = "/nologinstatus", method = RequestMethod.GET)
	public String checkpackage(ModelMap model,@RequestParam int track) {
		System.out.println("track");
		System.out.println(track);
		ArrayList<String> list = new ArrayList<String>();
		loginService.getStatus(list, track);
		if(list.size()!=0) {
		model.put("pack",list.get(0));
		model.put("truckid",list.get(1));
		model.put("deliver",list.get(2));
		model.put("des",list.get(3));
		return "nologinresult";
		}
		else {
			return "signuperror";
		}
	}
	
	@RequestMapping(value = "/list-products", method = RequestMethod.GET)
	public String listproducts(ModelMap model) {
		System.out.println("getting first:");
		model.addAttribute("products", loginService.listproducts());
		return "products";
	}
	
	@RequestMapping(value = "/place-order", method = RequestMethod.GET)
	public String showplaceorder(ModelMap model, @RequestParam String itemid,@RequestParam String des) {
		model.put("des",des);
		System.out.println(des);
		model.put("itemid",itemid);
		model.put("userid",this.myuser.getUserid());
		model.put("upsid",this.myuser.getUpsid());
		model.put("upspassword",this.myuser.getUpspassword());
		model.put("desx",this.myuser.getDesx());
		model.put("desy",this.myuser.getDesy());
		return "place-order";
	}
	
	@RequestMapping(value = "/order-pdf", method = RequestMethod.GET)
	public ModelAndView generatePdf(HttpServletRequest req, HttpServletResponse rep, @RequestParam String des,@RequestParam Integer quantity,@RequestParam String upsid, @RequestParam Integer desx, @RequestParam Integer desy ){
		System.out.println(des);
		return loginService.generatePdf(this.order,req, rep);
	}
	
	@RequestMapping(value = "/place-order", method = RequestMethod.POST)
	public String placeorder(ModelMap model, @RequestParam String des,@RequestParam String itemid,@RequestParam Integer quantity,@RequestParam String upsid, @RequestParam String pw, @RequestParam Integer desx, @RequestParam Integer desy ) {
		this.order.setDes(des);
		this.order.setQuantity(quantity);
		this.order.setUpsid(upsid);
		this.order.setDesx(desx);
		this.order.setDesy(desy);
		int submit = loginService.submit(des,itemid,quantity,upsid,pw,desx,desy);
		if(submit!=-1) {
			String id = loginService.track(itemid,quantity,upsid,desx,desy );
			System.out.println(id);
			model.put("track",id);
			model.put("des",des);
			model.put("quantity",quantity);
			model.put("upsid",upsid);
			model.put("desx",desx);
			model.put("desy",desy);
			this.order.setId(id);
			return "submitted";
		}
		else {return "error";}
	}
	@RequestMapping(value = "/signedup", method = RequestMethod.POST)
	public String  register(@RequestParam String username,@RequestParam String password,@RequestParam Integer upsid,@RequestParam String upspassword, @RequestParam Integer desx, @RequestParam Integer desy) {
		Boolean success = loginService.register(username,password,upsid,upspassword,desx,desy);
		if(success==true) {
			return "login";
		}
		else {
			return "signuperror";
		}
	}

	
	@RequestMapping(value = "/signup", method = RequestMethod.GET)
	public String signup() {
		return "signup";
	}
	
	
	@RequestMapping(value = "/signup", method = RequestMethod.POST)
	public String handleSignup() {
		return "signup";
	}
	
	@RequestMapping(value = "/login", method = RequestMethod.POST)
	public String handleUserLogin(ModelMap model, @RequestParam String name,
			@RequestParam String password) {
		if (!loginService.validateUser(name, password)) {
			model.put("errorMessage", "Invalid Credentials");
			return "login";
		}
		loginService.getmyuser(this.myuser,name);
		System.out.println(this.myuser.getDesx());
		System.out.println(this.myuser.getDesy());
		return "redirect:/list-products";
	}
}
