import psycopg2
import world_amazon_pb2
import time
import sys
import socket
import gl
from google.protobuf.internal.decoder import _DecodeVarint32
from google.protobuf.internal.encoder import _EncodeVarint

#=========TODO==========
#    update status in orders

#  Request table :  request_id   request   status

WORLD_HOST = gl.worldhost
WORLD_PORT = gl.worldport

def check_q():
    with gl.lock:
        size = gl.q.qsize()
        print("=========")
        for i in range(size):
            r = gl.q.get()
            print(r)
            gl.q.put(r)
        print("=========")


class sender():
    def __init__(self):
        self.conn = None
        self.sock = None
        self.reconnect = False
        self.ACommands = world_amazon_pb2.ACommands()
        
    def setup(self,conn,sock):
        self.conn = conn
        self.sock = sock   

    def handle(self):
        # seq = 0 is ACK to world responses
        print("sender running")
        while(1):
                if(gl.q.empty()!=True):
                    check_q()
                    with gl.lock:
                        size = gl.q.qsize()
                        print("send size is:",size)
                    for i in range(size):
                        with gl.lock:
                            req = gl.q.get()
                            if(req[0]!=0):
                                gl.q.put(req)
                        cmd = req[1]
                        print("---------sending----------")
                        print(cmd)
                        self.send_ACommands(cmd.SerializeToString())
                    time.sleep(5)
                else:
                    time.sleep(3)

    def send_ACommands(self,msg):
        try:
            hdr = []
            _EncodeVarint(hdr.append,len(msg))
            self.sock.sendall(hdr[0])
            self.sock.sendall(msg)
            print("sent")
        except BaseException:
            print("======= send ACommand error=========")
            print("Unexpected Sender_socket error: ", sys.exc_info()[0])
            #print ("Caught exception socket.error : %s" % exc)
            self.sock.close()
            sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
            sock.connect((WORLD_HOST, WORLD_PORT))
            print("reconnected")
            self.sock = sock
            self.reconnect = True

    # def db_insert_request(self,seq,request,type):
    #         sql = ("""INSERT INTO requests (seq_num,request,status,type) VALUES(%s,%s,%s,%s);"""%(seq, psycopg2.Binary(request),"'unacked'",type))
    #         cur = self.conn.cursor()
    #         cur.execute(sql)
    #         self.conn.commit()
    #         cur.close()


    # def append_request(self,result,request): #res is byte string    
    #             type = result[1]
    #             string = None
    #             #======error===========
    #             print("string is :",string)
    #             if(type=="APack"):
    #                 pack = (self.ACommands).topack.add()
    #                 pack.ParseFromString(string)
    #             elif(type == "APutOnTruck"):
    #                 load = (self.ACommands).load.add()
    #                 load.ParseFromString(string)
    #             elif (type == "APurchaseMore"):
    #                 purchase = (self.ACommands).buy.add()  
    #                 purchase.ParseFromString(string)
    #                 print("the purchase is", purchase)
    #             elif(type == "AQuery"):
    #                 query = (self.ACommands).queries.add()
    #                 query.ParseFromString(string)
    #             elif(type == "ACK"): # the ack for world's response
    #                 ack = (self.ACommands).acks.add()
    #                 ack.ParseFromString(string)
    #             else:
    #                 print("error type when appending request:",type)


    # def check_ack(self,req):
    #         seq = req[0]
    #         with gl.rlock:
    #             size = gl.rq.qsize()
    #         acked = True
    #         for i in range(size):
    #             response = gl.rq.get()
    #             if(seq != response):
    #                 gl.rq.put(response)
    #             else:
    #                 acked = False
    #         if(acked==False):
    #             gl.q.put((req[0],req[1]))
    #             gl.rq.put(req[0])

        
